const { cars } = require('../../models')
const { Op } = require('sequelize')
const imagekit = require('../../lib/imageKit')

const createDataCarImage = async (req, res) => {
    try {

    const { name, price, type, image} = req.body;

        // req.body.name, req.body.price, req.body.quantity
        // untuk dapat extension file nya
        const split = req.file.originalname.split('.')
        const ext = split[split.length - 1]

        // upload file ke imagekit
        const img = await imagekit.upload({
            file: req.file.buffer, //required
            fileName: `${req.file.originalname}.${ext}`, //required
        })
        console.log(img.url)
        
    const newCars = await cars.create({
        name,
        price,
        type,
        image: img.url
    })
        res.redirect('/admin/datacar')
    
    } catch (error) {
            res.status(400).json({
            status: "fail",
            message: error.message
        })
    }
}

const updateDataCarImage = async (req, res) => {
 try {
        const { name, price, type } = req.body;
    const id = req.params.id;
    
        const split = req.file.originalname.split('.')
        const ext = split[split.length - 1]

        // upload file ke imagekit
        const img = await imagekit.upload({
            file: req.file.buffer, //required
            fileName: `${req.file.originalname}.${ext}`, //required
        })
 
    await cars.update(
        {
         name,
        price,
        type,
        image: img.url
        },
        {
            where: {
                id,
            },
        }
    );
    res.redirect('/admin/datacar');
    } catch (error) {
                 res.status(400).json({
            status: "fail",
            message: error.message
        })
    }
}

const homepage = async (req, res) => {
    res.render('index')
}

const dataCar = async (req, res) => {
    const items = await cars.findAll()
    res.render('index', { items })
}

const createPage = async (req, res) => {
    res.render('newdata')
}
const createDataCar = async (req, res) => {
    const { name, price, type } = req.body
    const newCars = await cars.create({
        name,
        price,
        type
    })
    res.redirect('/admin/datacar')   
}

const deletePage = async (req, res) => {
    res.render('update')
}

const search = async (req, res) => {
    const id = req.params.id
    const name = req.params.name
    
    const query = req.query.q
    const allQuery = query.split(' ')
    const comparison = [];

    for(let i = 0; i < allQuery.length; i++){
        comparison.push({
            name : {
                [Op.iLike] : '%' + allQuery[i],
            },
        });
        comparison.push({
            type : {
                [Op.iLike] : '%' + allQuery[i],
            },
        });
    }

    const items = await cars.findAll({
        where: {
            [Op.or] : comparison,
        }
    })

    res.render('index', {
       id,
       name,
       items,
       query
    })
}

const deleteDataCar = async (req, res) => {
    const id = req.params.id
    await cars.destroy({
        where: {
            id
        }
    })
    res.redirect('/admin/datacar')
}

const updatePage = async (req, res) => {
    const items = await cars.findByPk(req.params.id)
    res.render('update', {
        items
    })
}

const updateDataCar = async (req, res) => {
    const { name, price, type } = req.body;
    const id = req.params.id;
    await cars.update(
        {
            name,
            price,
            type
        },
        {
            where: {
                id,
            },
        }
    );
    res.redirect('/admin/datacar');
};

const filterType = async (req, res) => {
    const id = req.params.id
    const type = req.params.type
    const name = req.params.name

    const query = req.query.q
    const items = await cars.findAll({
        where: {
            type: query
        }
    })

    res.render('index', {
       id,
       name,
       type,
       items,
       query
    })
}

module.exports = {
    homepage,
    createPage,
    dataCar,
    createDataCar,
    deletePage,
    deleteDataCar,
    updatePage,
    updateDataCar,
    filterType,
    search,
    createDataCarImage,
    updateDataCarImage
}
