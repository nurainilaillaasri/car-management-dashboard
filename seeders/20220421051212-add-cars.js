'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('cars', [{
      "name": "Toyota",
      "type": "Medium",
      "price": 300000,
      "image": "https://ik.imagekit.io/rizkioktav70/default-image.jpg?",
      "updatedAt": "2022-04-21T05:16:01.137Z",
      "createdAt": "2022-04-21T05:16:01.137Z"
    }, {
      "name": "Honda",
      "type": "Small",
      "price": 200000,
      "image": "https://ik.imagekit.io/rizkioktav70/default-image.jpg?",
      "updatedAt": "2022-04-21T05:16:01.137Z",
      "createdAt": "2022-04-21T05:16:01.137Z"
    }], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
