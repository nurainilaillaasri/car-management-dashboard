const router = require('express').Router()
const cars = require('../controller/carController')
const adminCars = require('../controller/adminController/carController')
const Upload =  require('../controller/uploadController')

// middleware
const uploader = require('../middlewares/uploader')

// API server
router.post('/api/cars',uploader.single('image'), cars.createCars)
// router.post('/api/cars', cars.createDataCar)
router.get('/api/cars', cars.findDataCar)
router.get('/api/cars/:id', cars.findDataCarById)
router.put('/api/cars/:id', cars.updateDataCar)
router.delete('/api/cars/:id', cars.deleteDataCar)


//Admin
router.get('/admin', adminCars.homepage)
router.get('/admin/datacar', adminCars.dataCar)
router.get('/admin/createpage', adminCars.createPage)
router.post('/admin/addnewcar',uploader.single('image'), adminCars.createDataCarImage)
// router.post('/admin/addnewcar', adminCars.createDataCar)
router.post('/admin/datacar/:id', adminCars.deleteDataCar)
router.get('/admin/datacar/updatepage/:id', adminCars.updatePage)
router.post('/admin/datacar/updatedata/:id',uploader.single('image'), adminCars.updateDataCarImage)
// router.post('/admin/datacar/updatedata/:id', adminCars.updateDataCar)
router.get('/admin/search', adminCars.search)
router.get('/admin/filter/:type', adminCars.filterType)

// test upload image 
router.post('/api/upload', uploader.single('image'), Upload.uploadImage)

module.exports = router